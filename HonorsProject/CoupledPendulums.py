# -*- coding: utf-8 -*-
"""
Created on Sat Apr 21 19:02:15 2018

@author: otowe
"""

import numpy as np
import matplotlib.pyplot as plt

#defining intial parameters
#Here everything can be used as a global variable other than the f term,
#which is contigent on the magnitude of Theta
#(See paper equation 5)


g, h= 9.808, 0.00002 #h is step size
Length_p1, Length_p2 = 1.0, 1.0
m1, m2 = 1.0, 1.0 #mass of each pendulum
M = 0.1 #mass of each attachment point
ro = 0.02 #dampening constant
x1, x2 = 0.0, 0.0 #initial distortion of attachment points
lamda = 0.1 #constant, again see equation 5
k = 10.0 #spring constant
Thetatilde = 0.1 #constant, see equation 5


def EulerTheta(diffeq, Theta, t, h):
    """approximates Theta by adding very small increments to Theta(t)
    input ===
    diffeq, Theta value, time, and step size
    output ===
    function Theta with new, small increment from step size"""
    
    dThetadt = diffeq(Theta, t)
    return (Theta + h*dThetadt)  


def pendulum (Theta,t):
    """Pendulum Function will generate Functions of Theta, dThetas_dt, functions of x, and dx_dt
    using information from equations 3,6 and 7. The second derivatives with respect to time
    were reduced to a system of first order ODE's using variable substitutions
    
    input ===
    All global values, Initial values for second derivatives of Theta functions and x functions
    (using variable substitution)
 
    f values determined based on equation 5 in paper
    
    When Theta(t) < Thetatile (constant), f = -2*lamda (lamda is a constant)
    
    Likewise, when Theta(t) > Thetatilde, f = 2*lamda
    
    output === 
    
    Functions of Theta, x, and the first derivatives of each function, and figures for each"""
    
    dThetadt = np.zeros(8)
    
    #Pendulum One
    
    #Theta1 dot definition
    dThetadt[0] = Theta[1]
    
    #Determining f value for Theta(1)
    if abs(Theta[0])< Thetatilde:
        f =  -2.0*lamda
    else:
        f = 2.0*lamda
        
    #Theta1 double dot    
    dThetadt[1] = (-f*Theta[1] - m1*g*Theta[0] - (m1/M)*(f*Theta[1] - 2*ro*Theta[5] + m1*g*Theta[0] +k*(Theta[6]-Theta[4])))/(m1*Length_p1) 
    
    
    #PendulumTwo
    
    #Theta2 dot definition
    dThetadt[2]= Theta[3]
    
    #Determing f value for Theta(2)
    if abs(Theta[2])< Thetatilde:
        f =  -2.0*lamda
    else:
        f = 2.0*lamda
        
    #Theta2 double dot    
    dThetadt[3] = (-f*Theta[3] - m2*g*Theta[2] - (m2/M)*(f*Theta[3] - 2*ro*Theta[7] + m2*g*Theta[2] -k*(Theta[6]-Theta[4])))/(m2*Length_p2)
    
    #x1 dot definition
    dThetadt[4] = Theta[5] 
    
    #Must use f value for Theta[0], redefine
    if abs(Theta[0])< Thetatilde:
        f =  -2.0*lamda
    else:
        f = 2.0*lamda
        
    #x1 double dot definition  
    dThetadt[5] = (f*Theta[1]-2*ro*Theta[5] + m1*g*Theta[0]+k*(Theta[6]-Theta[4])) / M
    
    #x2 dot definition
    dThetadt[6] = Theta[7]
    
    #Determing f value for Theta(2), again, must redefine from Theta[2]
    if abs(Theta[2])< Thetatilde:
        f =  -2.0*lamda
    else:
        f = 2.0*lamda
        
    #x2 double dot definition    
    dThetadt[7] = (f*Theta[3]-2*ro*Theta[7] + m2*g*Theta[2]-k*(Theta[6]-Theta[4])) / M
        
    return dThetadt



def go(t,t_final):    
    
    """Go function takes in initial time and final time you want to run the pendulums.
    With these times, you can generate figures for the phase solutions and antiphase solutions
    depending on the times and initial global values, as well as the phase diagrams for each pendulum 
    and their attachment points
    
    input ===
    global variables, initial and final time, initial values for Theta functions, dTheta functions,
    x functions, dx functions
    
    output ===
    list containing Theta Function values, dTheta values, x function values, dx function values
    and figures displaying functions of Theta, and Phase diagrams for Theta and x positons
    
    One very key thing, the phase diagrams in the paper were created for the figures from
    time 390.0<t<400.0, which is why if you just plot phase diagrams from beginning the look messy"""
    
    
    #Theta, dTheta, x, dx function values
    
    Theta = [0.2, 0.0, 0.3, 0.0, 0.0, 0.0, 0.0, 0.0]

    #Define empty list for numerical positions
    
    time,Theta1_approx,x1approx,Theta2_approx,x2approx = [],[],[],[],[]
    dTheta1_dt,dTheta2_dt,dx1,dx2=[],[],[],[]
    
    #append values to list as time passes
    while t<t_final:
        time.append(t)
        
        Theta1_approx.append(Theta[0]), Theta2_approx.append(Theta[2])
        
        dTheta1_dt.append(Theta[1]), dTheta2_dt.append(Theta[3])
         
        x1approx.append(Theta[4]), x2approx.append(Theta[6])
        
        dx1.append(Theta[5]), dx2.append(Theta[7])
        
        
        Theta1 = EulerTheta(pendulum, Theta, t, h)
        for i in range(len(Theta)):
            Theta[i] = Theta1[i]
        
        t = t + h
    
    #Figure will plot the positions of each pendulum using analytic and approximated values
    plt.figure(1)
    plt.plot(time,Theta1_approx, label = 'Theta 1')
    plt.plot(time, Theta2_approx, label = 'Theta 2')
    plt.title("Theta vs time for ro = %.2f and k = %.2f, t = %i"%(ro,k,t))
    plt.xlabel('t (s)')
    plt.ylabel('theta (t)')
    plt.ylim(-0.35,0.35)
    plt.legend(loc=1)
    #plt.xlim(48,50)
    #plt.savefig("(Theta vs time for ro = %.2f and k = %.2f, t = %i).png"%(ro,k,t))
    plt.show()

   #Phase Diagram for Theta Functions
    plt.figure(2)
    plt.plot(Theta1_approx,dTheta1_dt, label = 'Theta 1')
    plt.plot(Theta2_approx,dTheta2_dt, label = 'Theta 2')
    plt.xlim(-1,1)
    plt.ylim(-2.5,2.5)
    plt.xlabel("Theta Functions")
    plt.ylabel("Theta dot Functions")
    plt.title('Phase Diagram for Theta1 and Theta2 when ro = %.2f and k = %.2f, l = %.2f'%(ro,k,Length_p1))
    plt.legend()
    #plt.savefig("(Phase Diagram for Theta1 and Theta2 when ro = %.2f and k = %.2f, t = %i).png"%(ro,k,t))
    plt.show()
    
    
    #Phase Diagram for X functions
    plt.figure(3)
    plt.plot(x1approx,dx1, label = 'x1')
    plt.plot(x2approx,dx2, label = 'x2')
    plt.xlim(-1,1)
    plt.ylim(-2.5,2.5)
    plt.xlabel("x functions")
    plt.ylabel("x dot functions")
    plt.title('Phase Diagram for x1 and x2 when ro = %.2f and k = %.2f'%(ro,k))
    plt.legend()
    #plt.savefig("(Phase Diagram for x1 and x2 when ro = %.2f and k = %.2f, t = %i).png"%(ro,k,t))
    plt.show()
    
    #dTheta1_dt.remove(0), dTheta2_dt.remove(0)
    #Going to work on 
    #dTheta1dt = [ '%.3f' % elem for elem in dTheta1_dt ]
    #dTheta2dt = [ '%.3f' % elem for elem in dTheta2_dt ]
    
go(0.0,10.0)   