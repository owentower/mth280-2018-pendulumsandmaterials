Link to paper:
https://arxiv.org/pdf/0802.1024.pdf

How the code works

The Coupled Pendulums program essentially is an ODE solver that breaks down two second order ODE's into
multiple single order ODE systems. This was done by using equations five, six, and seven in the paper and solving for
x double dot and theta double dot. Then, by saying that these second derivative functions were the first time derivatives 
of the theta dot and x dot,the program was able to take the initial values for the first and second time derivatives
 of theta and x and generate the synchronization patterns of pendulums in small areas of phase space. 

With the code generating Theta functions of each pendulum, and their corresponding phase diagrams, it is very easy to change
some initial parameters to see how changes in things like the length of each pendulum, initial positions, or spring constants
change the in phase or anti-phase synchronization patterns of the pendulums.

With all of these parameters being global variables that are defined at the beginning of the program, the only additional thing that must be done to change the code 
is change the final time, or in other words, how long the program runs. For example, if the pendulums don't exhibit any distinct synchronization patterns after 
ten seconds, the user might want to change the final time input of the go function in order to see if any patterns develop over an extended period of time.
Since this happens very frequently with the pendulums, the x-limits and y-limits of the plots are commented out in the plotting parts of the function for the user to be able
to change them to be able to see only parts of the pendulum system that they want.


What is being modeled from the paper


The paper that this was modeled off of contained figures that demonstrated the synchronization patterns of pendulums
that were subject to changes in either the dampening constant of the pendulum system, or the initial positions of each pendulum.
The figures that correspond to the anti-phase synchronization of the pendulums are figures a-d in figure two of the paper, which were duplicated in figures
A-D of the the results folder. The paper figures that demonstrated in-phase synchronization are in figures a-d in figure three, which were duplicated in figures 
E-H in the results figure.

The successful duplication of the figures in the paper indicated that the program works properly, and allowed for an extension beyond what was presented in the paper
by experimenting with different parameters to see how they affected synchronization patterns.



Extension

While the paper did provide a way to model the synchronization pattern of pendulums, it did not explore how changing certain parameters would affect
how the coupled pendulums would interact with each other. All figures not labeled A-H in the results folder correspond to how changing certain parameters affect the 
synchronization patterns of the pendulums. For example, making each pendulum 10 times the length of those in the original in-phase synchronization pattern caused the 
pendulums to synchronize not in a few seconds, but in just over a minute. While this is an interesting result, it is not the same case for the anti-phase 
synchronization patterns. When all of the same initial conditions were in place as the initial anti-phase synchronization pattern, making each pendulum have a length
that was ten times as large as the original did not affect the period for synchronization as much, as it still only took a few seconds for this to happen. 

Additional exploration was done with these initial parameters, such as changing the spring constant of the board connecting the two pendulums, and changing the initial
positions of the pendulums. 



Looking further beyond

Going forward, more and more pendulums could be added to the program to show how more than two harmonic oscillators interact when there is a coupling mechanism between them. 
While this doesn't seem very significant, the applications of this program can include things such as planetary orbits, bird migration patterns, earthquake detection, and much more. 
All of these systems act as coupled harmonic oscillators in nature, and by changing certain parameters (example being number of planets in solar system) of the system, 
one can determine the synchronization pattern of the objects acting in the system. 

While this program was just a simple stepping stone into the coupled harmonic oscillator phenomena, its applications extend far beyond two coupled pendulums.