#Sample Demo for Material Properties Module

import matprop as mp

#Scenario 1 is a 100 lb force applied perpendicular to cross section of a sugar pine wooden beam, the force points towards the center of the beam. 
#It is 10 ft long and has square cross-section length of .5 ft. It's elastic modulus is 1.19x10^9 psi
Area = .5**2
print Area

Stress = mp.stress(100, Area)
Strain = mp.strain(Stress, 1.19*10**9)
print "Stress is %i"%(Stress)
print "Strain is %.10e"%(Strain)



#Scenario 2 is a sugar pipe beam sitting horizontally on two supports at either end of the beam (neglect its own weight for force calculations). 
# the sugar pine has a square cross-section that is .25m by .25m. 
#There is a 50 N force applied directly in the center of the beam pointing vertically down.
#We want to calculate the shear stress on the beam at the center of the beam.
#shear force along the beam is 50 N all the way through the beam. 
V = 50 
#Q is defined as the first moment of area, with an equation of A*y
#where A is the area about the point we are focusing on and y is the distance between the center of gravity of that area and the center of gravity of the cross-section
#since the cross-section is a square, the center of gravity is at the center of the square. the area above that point is half the total area.
area_cross = .25*.25
Area = area_cross/2
#the center of gravtiy of half the area of a square is in the center of that area, so it would be 1/4 the length of the square above the center of the square
# y is therefore 1/4 the length of one side
y = .25/4
#Q = Area * y (from equation above)
Q = Area*y
#rotational Inertia (I) is equal to b*h^3/12
I = .25*.25**3/12
#the thickness of the section is just the width of the square, .25
t = .25
Shear_Stress = mp.ssib(V,Q,I,t)
print "Shear Stress in the wooden beam is %i"%(Shear_Stress)