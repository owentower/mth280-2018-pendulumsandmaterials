"""
MODULE OF MATERIAL RELATIONSHIPS FOR SINGLE BEAM

UNITS
    Metric System:
        Quantity                    Unit Name               Unit Abbreviation
        
        Force                       Newtons                 N
        Length                      Meter                   m
        Area                        Square Meters           m^2
        Stress                      Pascals                 Pa or N/m^2
        E                           Pascals                 Pa or N/m^2
        G                           Pascals                 Pa or N/m^2
        Poisson's Ratio             Unitless                Unitless
        Strain                      Unitless                Unitless
        Moment of Inertia           Meters to fourth power  m^4
        Moment                      Newtons-Meters          N*m
        Torque                      Newtons-Meters          N*m
        Polar Moment of Inertia     Meters to fourth power  m^4
        First Moment of Area        Meters cubed            m^3
        
    Imperial System:
        Quantity                    Unit Name               Unit Abbreviation
            
        Force                       Pounds                  lb
        Length                      Feet                    ft
        Area                        Square feet             ft^2    
        Stress                      Pounds per square inch  psi or lb/(in^2)
        E                           Pounds per square inch  psi or lb/(in^2)
        G                           Pounds per square inch  psi or lb/(in^2)
        Poisson's Ratio             Unitless                Unitless
        Strain                      Unitless                Unitless
        Moment of Inertia           Feet to fourth power    ft^4
        Moment                      Foot-Pounds             lb-ft
        Torque                      Foot-Pounds             lb-ft
        Polar Moment of Inertia     Feet to fourth power    ft^4
        First Moment of Area        Feet cubed              ft^3
        
DETERMINING IF A QUANTITY IS POSITIVE OR NEGATIVE
    Force:                  A force is positive if the force is in the direction of the positive axes.
    Normal Stress:          Normal Stress is positive if the beam is in tension.
    Shear Stress:           Cut the beam into sections. If the right-most point on each section has a shear force pointing in the negative direction, shear stress is positive.
    Normal Strain:          Normal Strain is positive when normal stress is positive (the beam is getting longer).
    Shear Strain:           Shear Stress is positive when the shear stress is positive.
    Moment:                 A reactionary moment that causes the beam to be in compression at the top is a positive moment.
    Torque:                 Torque is positive when the torque would cause counter-clockwise rotation.
    Moment of Area:         Positive when the point in question is above the center of Gravity, Negative when the point is below the center of gravity.
    Angle of twist:         Positive when torque is positive, negative when torque is negative.
    Moduli:                 (Elastic Modulus, Shear Modulus, and Poisson's Ratio) are always positive.
    Moments of Inertia:     Always positive. 
    
Common Cross-sectional shapes and their Moments of Inertia:
    Circle: 
        I = pi*r^4/4                            where r is the radius of the circle 
        J = pi*r^4/2                            
    Hollow Circle (Hollow cylinder):
        I = pi*(R1)^4/4- pi*(R2)^4/4            where R1 is the radius from center to outermost edge and R2 is radius from center to innermost edge.
        J = pi*(R1)^4/2- pi*(R2)^4/2            
    Rectangle:
        Ix = b*h^3/12                           
        Iy = b^3*h/12                           where b is the length of the base and h is the length of the height
        J = b*h*(b^2+h^2)/12                    
"""

def stress(F=None, A=None, eps=None, E=None):
    """ Common function to calculate normal stress (stress perpendicular to cross-section).
        
        For units of quantities, see Module docstring
        
        INPUT
        =====
        F:      Sum of all forces perpendicular to the cross sectional area of the beam
        A:      Cross-sectional area of the material
        eps:    Strain of material
        E:      Young's Modulus
        
        All inputs are keyword arguments, so inputs are assigned in order of left to right unless
            you assign them using assignment statements: sig = number, E = number, etc
        
            Note: Only input one of these two options: 
                Fnet and A 
                    or
                E and eps
            
        Examples
        =====
        stress(100, 50)
        stress(A = 50, Fnet = 100)
        stress(eps = 25, E = 2)
        
        OUTPUT
        =====
        Normal Stress
    """   
    if (eps == None or E == None) and (F == None or A == None):
        return "Not Enough Information to Compute"
    elif eps == None or E == None:
        sig = F/A
        return sig
    else: 
        return E*eps 
       
    return "done"
def strain(sig=None, E=None, L0=None, Lf=None):
    """ Function used to calculate normal strain (strain perpendicular to cross-section).
    
        For units of quantities, see Module docstring
        
        Inputs
        =====
        sig:    Stress
        E:      Young's Modulus 
        L0:     Initial length of the beam
        Lf:     Final length of beam under load
        
        All inputs are keyword arguments, so inputs are assigned in order of left to right unless
            you assign them using assignment statements: sig = number, E = number, etc.
        
                Note: Only input one of these two options:
                    sig and E 
                        or
                    L0 and Lf 
        Examples
        =====
        strain(100, 50)
        strain(sig = 50, E = 100)
        strain(L0 = 25, Lf = 2)
        
        Output
        =====
        Strain
    """
    if (sig == None or E == None) and (L0 == None or Lf == None):
        return "Not Enough Information to Compute"
    elif sig == None or E == None:
        return (Lf-L0)/L0
    else:
        return sig/E

def youngs_modulus(sig, eps):
    """ Function used to approximate Young's (Elastic) Modulus of a material.
        
        For units of quantities, see Module docstring
        
        Inputs
        =====
        sig:    Normal Stress
        eps:    Normal Strain
        
        Note: For best accuracy, these inputs should be from the elastic region of stress-strain graph
    
        Output
        =====
        Young's Modulus (E)
    """
    return sig/eps

def shear_modulus(sig, eps):
    """ Function used to approximate Shear Modulus of a material.
        
        For units of quantities, see Module docstring
        
        Inputs
        =====
        Shear Stress (sig)
        Shear Strain (eps)
        
        Note: For best accuracy, these inputs should be from the elastic region of stress-strain graph
    
        Output
        =====
        Shear Modulus (G) 
    """
    return sig/eps

def poissons_ratio(E=None, G=None, enorm=None, eshear=None):
    """ Function used to calculate Poisson's ratio of a material.
        
        For units of quantities, see Module docstring
        
        Inputs
        =====
        E:      Young's Modulus
        G:      Shear Modulus
        enorm:  Normal Strain
        eshear: Shear strain
        
        All inputs are keyword arguments, so inputs are assigned in order of left to right unless
            you assign them using assignment statements: sig = number, E = number, etc.
        
            Note: Only input one of these two options: 
                  E and G 
                    or
                enorm and eshear
                
        Examples
        =====
        poissons_ratio(100, 50)
        poissons_ratio(E = 50, G = 100)
        poissons_ratio(et = 25, enorm = 2)
        
        Output
        =====
        Poisson's Ration (unitless value)
    """
    if (E == None or G == None) and (enorm == None or eshear == None):
        return "Not Enough Information"
    elif E == None or G == None:
        return abs(enorm/eshear)
    elif enorm == None or eshear == None:
        return E/(2*G)-1

def bending_stress(M,y,I):
    """
        Function used to calculate the normal stress due to bending of a beam.
        
        For units of quantities, see Module docstring
        
        Input
        =====
        M:  Reactionary Moment of the material
        y:  vertical distance away from the Center of gravity of the cross-section; vertical distance from the neutral axis
        I:  Moment of Inertia of the cross-section about the Neutral Axis (x-axis through the center of gravity)
        
        Output
        =====
        Normal stress
    """
    return M*y/I

def angle_twist(T,L,G,J):
    """
        Function used to calculate the angle of rotation of a section of a beam with respect to another point of the beam.
        
        For units of quantities, see Module docstring
        
        Input
        =====
        T:  Sum of all torques applied to the section
        L:  Length of the section
        G:  Shear Modulus of the material
        J:  Polar moment of inertia of the cross-section of the material
        
        Output
        =====
        Angle of twist (in radians)
        
        Additional Note: This Calculaton assumes a homogenous material with the same cross-section throughout the beam.
       
        If there are multiple sections with different materials or different cross-sections on the same beam, you can still use this calculation:
        Step 1:
            Divide the beam into sections such that along every point of the subsection, the material and cross-sections are the same.
        Step 2:
            For each section, calculate the angle of twist from one end to the other (use that subsection's T,L,G,J for calculations). 
        Step 3:
            The angle of twist from one end of the beam to the other is the sum of all angles of twist.
    """
    return T*L/(G*J)

def ssib(V,Q,I,t):
    """
        Function used to calculate the Shear Stress in a beam about a certain point. 
        Input
        =====
        V   Internal Shear Force
        Q   First moment of Area (calculated as the sum(area above or below the point * distance between center of gravity of that area to center of gravity of cross-section))
        I   Moment of Inertia of the cross-section about the Neutral Axis (x-axis through the center of gravity)
        t   Thickness of member at the point
        
        Output
        =====
        Shear Stress
    """
    return V*Q/(I*t)